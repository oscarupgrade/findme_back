const express = require('express'); 
const path = require('path');
const passport = require('passport');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const cors = require('cors');

require('./passport');
require('dotenv').config();
const db = require('./db.js');
db.connect();

//Variables para rutas
const authRouter = require('./routes/auth.routes');
const forgotRouter = require('./routes/forgot.routes');
const savePhotoProfile = require('./routes/user.routes');
const addObjects = require('./routes/objects.routes');

const PORT = process.env.PORT || 5000; 
const app = express();


app.use((req, res, next) => {
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.use(cors({ origin: 'http://localhost:3000' }));


// Convierte el body de la petición y lo añade al objeto request
app.use(express.json());
// Convierte el body y lo añade al objeto request
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

//cookies de sesion
app.use(
    session({
      secret: process.env.SESSION_SECRET,
      resave: false,
      saveUninitialized: false,
      cookie: {
        maxAge: 360000,
        // httpOnly: false,
        // secure: false,
        // sameSite: false,
      },
      store: new MongoStore({ mongooseConnection: mongoose.connection }),
    })
    
    );
    
  app.use(passport.initialize());
  app.use(passport.session());
    
// rutas 
app.use('/auth', authRouter);
app.use('/forgot', forgotRouter);
app.use('/profile', savePhotoProfile);
app.use('/object', addObjects);


//middleware gestión de errores
app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
});

app.use((err, req, res, next) => {
    console.log(err);
    return res.status(err.status || 500).render('error', {
        message: err.message || 'Unexpected error',
        status: err.status || 500
    });
});

app.listen(PORT, () => { 
console.log(`Server running in http://localhost:${PORT}`); });
