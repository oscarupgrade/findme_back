const passport = require('passport');
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require('bcrypt');
const User = require('./models/User');

const saltRound = 10; //para criptar la contraseña

const validate = email => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

const validatePassword = password => {
    const res = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;   //Inicio de regex "/^" .
                                                                    //Si existe al menos un número "(?=.*\d)" .
                                                                    //Si existe al menos una minúscula "(?=.*[a-z])" .
                                                                    //Si existe al menos una mayúscula  "(?=.*[A-Z])" .
                                                                    //Que no existan espacios  "(?!.*\s)" .
                                                                    //Mminimo 8 caracteres y maximo 12  "{8,15}" .
                                                                    //Al menos un caracter especial
                                                                    //Fin de regex "$/"
    return res.test(String(password));
}

passport.serializeUser((user, done) => {
    return done(null, user._id);
  });
  
passport.deserializeUser(async (userId, done) => {
    try {
      const existingUser = await User.findById(userId);
      return done(null, existingUser);
    } catch (err) {
      return done(err);
    }
  });
  

passport.use(
    "register", 
    new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
        },
        async (req, email, password, done) => {
            //logica registro
          try {
            const validEmail = validate(email);
            if(!validEmail) {
                const error = new Error('Invalid email');
                return done(error);
            };
            //Si el email existe no registra
            const previousUser = await User.findOne({email: email.toLowerCase()});
            
            if(previousUser){
                const error = new Error('El email ya existe');
                return done(error);
            };

            //si el username existe no registra
            // const previousUserName = await User.findOne({userName: userName});
            
            // if(previousUserName){
            //     const error = new Error('El nombre de usuario ya existe');
            //     return done(error);
            // };

            //validación contraseña con expresion regular
            const validPass = validatePassword(password);
            if(!validPass){
                const error = new Error('Invalid password');
                return done(error);
            };

            const hash = await bcrypt.hash(password, saltRound);
           
            const newUser = new User({
                email: email.toLowerCase(),
                password: hash,
                passwordConfirm: req.body.passwordConfirm && hash, 
                userName: req.body.userName,
            });
            
            const savedUser = await newUser.save();
            
            return done(null, savedUser);
          } catch (error) {
              return done(error);
          }
        }
    )
);

passport.use(
    'login',
    new LocalStrategy (
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
        },
        async (req, email, password, done) => {
            try {
                const currentUser = await User.findOne({email: email.toLowerCase()});

                if(!currentUser) {
                    const error = new Error('El usuario no existe');
                    return done(error);
                }


                const isValidPassword = await bcrypt.compare(
                    password,
                    currentUser.password
                );

                if(!isValidPassword) {
                    const error = new Error('The email or password is invalid!');
                    return done(error);
                }

                return done(null, currentUser);

            } catch (error) {
                return done(error);
            }
        }
    )
    );


    // passport.use(
    //     'sendEmailPassword',
    //     new LocalStrategy (
    //         {
    //             usernameField: 'email',
    //             passwordField: 'password',
    //             passReqToCallback: true,
    //         },
    //         async (req, email, done) => {
    //             try {
    //                 const currentUser = await User.findOne({email: email.toLowerCase()});
    
    //                 if(!currentUser) {
    //                     const error = new Error('El usuario no existe');
    //                     return done(error);
    //                 }
    //                 return done(null, currentUser);
    
    //             } catch (error) {
    //                 return done(error);
    //             }
    //         }
    //     )
    //     );
