const express = require('express');
const userController = require('../controllers/user.controller');
const router = express.Router();


router.put('/save_profile', userController.savePhotoProfile);

module.exports = router;