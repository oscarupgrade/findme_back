const express = require('express');
const forgotController = require('../controllers/forgot.controller');
const router = express.Router();


router.post('/sendemail', forgotController.emailPassword);
router.post('/sendforgot-email', forgotController.confirmCount);



module.exports = router;