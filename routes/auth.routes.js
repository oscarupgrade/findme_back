const express = require('express');
const authController = require('../controllers/auth.controller');
const authMiddleware = require('../middleware/auth.middleware');
const router = express.Router();

router.post('/register', authController.userRegister);
router.post('/login', authController.userLogin);
router.post('/send-email', authController.confirmCount);

router.get('/logout', authController.userLogout);
router.get('/check-session', authController.userCheck);

// router.put('/forgot-password', authController.userForgot);
// router.post('/forgot-password', authController.userForgotPassword);

module.exports = router;
