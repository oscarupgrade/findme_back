const express = require('express');
const objectsController = require('../controllers/objects.controller');
const fileMiddleware = require('../middleware/file.middleware');
const router = express.Router();


router.post('/add-object',[fileMiddleware.upload.single("image"), fileMiddleware.uploadToCloudinary], objectsController.objectsCreate);
router.get('/productsUser', objectsController.objectsGetUser);


module.exports = router;