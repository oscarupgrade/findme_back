const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const objectsSchema = new Schema(
    {
        group: {
            enum: ['electrónica', 'llaves', 'transporte', 'documentos personales', 'dinero', 'ropa', 'otros'],
            type: String,
            required: true
        },

        name: {
            type: String,
            required: true,
        },

        image: {
            type: String,
        },

        description: {
            type: String,
             required: true
        },
    },
    {timestamps: true},
);

const Objects = mongoose.model('Objects', objectsSchema); 

module.exports = Objects;
