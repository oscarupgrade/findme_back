const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema (
    {
        email: {type: String, required: true},
        password: {type: String, required: true},
        passwordConfirm: {type: String, required: true},
        userName: {type: String, required: true},
        img: { type: String, require: true },
        role: {
            enum: ['admin', 'user'],
            type: String,
            default: 'user',
            required: true
        },
        objects: [{
            type: mongoose.Types.ObjectId, 
            ref: 'Objects',//nombre que se pone segun el nombre de la base de datos    
        }],
    

    },

    {timestamps: true}
);

const User = mongoose.model('Users', userSchema);

module.exports = User;