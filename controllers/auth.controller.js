const passport = require("passport");
const User = require('../models/User');
const nodemailer = require("nodemailer");
// const {OAuth2Client} = require('google-auth-library')
require('dotenv').config(); 

const userRegister = (req, res, next) =>{
        passport.authenticate("register",(error, user) => {
            if (error){
                return res.status(401).json(error);
            };
    
            req.logIn(user, (error) =>{       
                // console.log(user)
                if (error){
                    return res.status(401).json(error);
    
                } else {
                    confirmCount(req, user.email);
                    return  res.status(200).json({ data: req.user, message: 'User registered successful' })
                }
            });
    
        })(req, res, next);
    
};

const userLogin = (req, res, next) => {
    passport.authenticate('login',{}, (error, user) => {   
        if (error){
            next(error);
        };

        req.logIn(user, (error) => {
            if (error) {
                next(error);
            } else {
                return res.status(200).json({ data: req.user, message: 'Login successful' });
            }
        });

    })(req, res, next);
};

const userCheck = async (req, res) => {
    if (req.user){
        return res.status(200).json({ data: req.user, message: null });
    } else {
        return res.status(200).json({ data: null, message: 'No user found' });
    }
  };

  const userLogout = (req, res) => {
    if (req.user) {
        req.logout();
        req.session.destroy(() => {
            res.clearCookie("connect.sid");
            return res.status(200).json({ data: null, message: 'Logout successful' });
        });
    } else {
        return res.sendStatus(401).json({ data: null, message: 'Unexpected error' });
    }
};

const confirmCount = (req, email) =>{
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'findmeapp21@gmail.com',
            pass: `${process.env.NODEMAILER_PASSWORD}`         
        }
    });

    const mailOptions ={
        from: "FindMe",
        to: email,
        subject: 'Validacion Email',
        html: `
        <h1>FindMe</h1>
        <p>¡¡¡Bienvenido a la familia FindMe!!!. Desde ahora podrás encontrar los objetos que hayas perdido...
        Por favor verifica el email en este enlace <a href="${req.headers.origin}/home">Verificar Cuenta</a></p>
        <p>¡¡¡Gracias!!!</p>
        `
    };

    transporter.sendMail(mailOptions, (error, info) =>{
        if(error) {
        //    console.log('nodemailer', error) 
         res.status(500).send(error.message);
        } else {
            //  console.log('email enviado', info);
            res.status(200).json(req.body)
        }
    })
}

// const userForgot = async (req, res, next) => {
   
// }

module.exports ={
    confirmCount,
    userCheck,
    userRegister,
    userLogin,
    userLogout,
    // userForgot
};