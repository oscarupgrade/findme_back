const User = require('../models/User');
const Objects = require('../models/Objects');

const objectsCreate = async (req, res, next) => {
    try {
        const {
            group,
            name,
            // image,
            description,
            
        } = req.body;

        const newObject = new Objects ({
            group,
            name,
            image: req.file_url,  // req.file? `/uploads/${req.file.filename}` : null sustituir en caso de que no vaya cloudinary
            description,
            
        });            
        const createdProduct = await newObject.save();
        
        return res.status(200).json(createdProduct);
   
    } catch (error) {
        return next(error);
    }
};


//trae todos los objetos para la home
const objectsGet = async (req, res, next) => {
    try {
        const objectsFinded = await Objects.find();

        return res.status(200).json(objectsFinded);
    
      } catch (error) {
        next(error);
      }
    };

    
    //objetos que ha añadido el usuario
  const  objectsGetUser = async (req, res, next) => {
      const {id} = req.user;
         try {
            const objectsUser = await Users.findById(id).populate('objects');
            return res.status(200).json(objectsUser);
        } catch (error) {
            next(error)
            }
     };



module.exports={
    objectsCreate,
    objectsGet,
    objectsGetUser
}