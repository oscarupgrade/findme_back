const passport = require("passport");
const User = require('../models/User');
const nodemailer = require("nodemailer");
require('dotenv').config(); 


// const emailPassword = (req, res, next) => {
//     passport.authenticate('login',{}, (error, user) => {   
//         if (error){
//             next(error);
//         };

//         req.logIn(user, (error) => {
//             if (error) {
//                 next(error);
//             } else {
//                 confirmCount(req, user.email)
//                 return res.status(200).json({ data: req.user, message: 'Login successful' });
//             }
//         });

//     })(req, res, next);
// };

const emailPassword = async (req, res, next) => {
const {email} =  req.body;
    try {
        const user = await User.find(req.user);
        if(user){
        const send = await User.find({email})
        confirmCount(req, email);
        return res.status(200).json(send);
        }
            
        
    } catch (error) {
        return next(error)

    }(req, res, next);
}

const confirmCount =(res, req, email) =>{
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'findmeapp21@gmail.com',
            pass: `${process.env.NODEMAILER_PASSWORD}`         
        }
    });

    const mailOptions ={
        from: "FindMe",
        to: 'oscar_1262@hotmail.com',
        subject: 'Validacion Email',
        html: `
        <h1>FindMe</h1>
        <h2>!Hola¡</h2>
        <p>Recientemente has solicitado que restablezcamos tu contraseña.
        Para cambiar tu clave de acceso a FindMe, sólo tienes que hacer click <a href="http://localhost:3000/forgot/recoverPassword">Aqui</a>
        </p>
        <p>¡¡¡Te esperamos pronto!!!</p>
        <br>
        <br>
        <h3>FindMe Team</h3>
        `
    };

    transporter.sendMail(mailOptions, (error, info) =>{
        if(error) {
             console.log('nodemailer', error) 
        //  res.status(500).send(error.message);
        } else {
            //  console.log('email enviado', info);
            res.status(200).json(req.body)
        }       
    })
}

module.exports ={
    emailPassword,
    confirmCount
}