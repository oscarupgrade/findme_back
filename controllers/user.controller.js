const User = require('../models/User');

const savePhotoProfile = async (req, res, next) => {
    try{
        const { img } = req.body;
        let message = '';

        if (req.user){
            await User.findByIdAndUpdate(
                req.user._id,
                { img: img }
            );
            message = 'Image saved';
        } else {
            message = 'Error, user not logged in';
        }

        return res.status(200).json({ message });

    } catch (err) {
        next(err);
    }
};

module.exports={
    savePhotoProfile
}
