	const mongoose = require('mongoose');
	require('dotenv').config();
	
	
	const DB_URL =  `mongodb+srv://${process.env.MONGO_DB_USER}:${process.env.MONGO_DB_PASSWORD}@cluster0.jfgjr.mongodb.net/findme?retryWrites=true&w=majority` || 'mongodb://localhost:27017/findme'  ; //'mongodb://localhost:27017/findme'
	
	const connect = () => {
	mongoose.connect(DB_URL, {
	  useNewUrlParser: true,
	  useUnifiedTopology: true,
	  useFindAndModify: false 
	})
	.then( () => {                
	    console.log('Connected to DB');
	})
	.catch(() => {
	    console.log('Error connecting to DB');
	});
}
	module.exports = {DB_URL, connect};
